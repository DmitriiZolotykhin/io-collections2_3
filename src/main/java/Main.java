import java.util.ArrayList;
import java.util.concurrent.SynchronousQueue;

/**
 * В примере создаются два потока, которые работают с очередью SynchronousQueue.
 * если  потребитель не запущен, то  производитель будет заблокирован и наоборот
 * Плюсы:
 * Эта очередь не пропускает null-данные. Попытка добавить null элемент кинет NullPointerException.
 * если  потребитель не запущен, то  производитель будет заблокирован и наоборот
 * SynchronousQueue блокируется,  до тех пор пока одна нить не будет готова взять данные, другая будет пытаться положить данные.
 * <p>
 * <p>
 * <p>
 * Минусы:
 * Не гарантирует очередности
 * Вы не сможете использовать iterator для SynchronousQueue, в ней нет элементов.
 **/


public class Main {

    public static void main(String args[])


    {

        final SynchronousQueue queue = new SynchronousQueue();
        Thread producer = new Thread("PRODUCER") {


            Event<String> event = new Event<String>("successfully");

            public void run() {


                try {
                    queue.put(event); // thread will block here
                    System.out.printf("[%s] published event : %s %n", Thread.currentThread()

                            .getName(), event);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        producer.start(); // starting publisher thread

        Thread consumer = new Thread("CONSUMER") {

            public Object event;

            public void run() {
                try {
                    //String event = (String) queue.take(); // thread will block here
                    event = queue.take();
                    System.out.printf("[%s] consumed event : %s %n", Thread.currentThread()
                            .getName(), event);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        consumer.start(); // starting consumer thread

    }
}
